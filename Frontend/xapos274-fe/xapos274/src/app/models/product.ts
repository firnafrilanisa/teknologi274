export interface Product {
    id: number;
    product_code: string;
    product_name: string;
    product_price: number;
    product_stock: number;
    variant_id: number;
    category_id: number;
}