export interface Category {
    id: number;
    category_code: string;
    category_name: string;
}