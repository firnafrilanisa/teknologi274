import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import { jsPDF } from 'jspdf';
import 'jspdf-autotable'

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})

export class CategoryComponent implements OnInit {
  public category: any = [];
  public tempCategory: any = [];
  public editCategory: Category;
  public deleteCategory: Category;
  public sortedBy = 'category_code'
  public mode = 'ASC'
  public current_page = 1;
  public records_per_page = 5;
  public multipleSelectIsEnabled = false;
  public selectedAll = false;
  public tempSelectedId: any[] = [];
  public searchCategory: any = [];

  constructor(private categoryService: CategoryService) {
    this.editCategory = {} as Category;
    this.deleteCategory = {} as Category;

  }

  ngOnInit(): void {
    this.getCategoryAPI();
    // console.log(this.category)
  }

  @ViewChild('tabelCategory', {static: false}) el!: ElementRef;

  createPDF() {
    let pdf = new jsPDF('p', 'pt', 'a4');
    pdf.html(this.el.nativeElement, {
      callback: (pdf) => {
      pdf.save("cetak.pdf");
      }
    })
  }

  public searchCategoryAPI(search: any): void {
    this.categoryService.searchCategory(search).subscribe(
      (response: any) => {
        if(search === '') {
          this.getCategoryAPI()
        } else {
          this.tempCategory = response.result;
        }
      });
  }

  public sortByCategoryAPI(sortedBy: string, mode: string): void {
    this.categoryService.sortingCategory(this.sortedBy, this.mode).subscribe(
      (response: any) => { 
      this.category = response.result;
      this.tempCategory = [];
      for (let i = (this.current_page - 1) * this.records_per_page; i < (this.current_page * this.records_per_page) && i < this.category.length; i++) {
        this.tempCategory.push(this.category[i])
      }
      });
  }


  public getCategoryAPI(): void {
    this.categoryService.getCategory().subscribe(

      (response: any) => {
        this.category = response.result;
        this.sortByCategoryAPI(this.sortedBy, this.mode)
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddCategory(addForm: NgForm): void {
    document.getElementById('add-category-form')?.click();

    this.categoryService.addCategory(addForm.value).subscribe(
      (response: Category) => {
        this.getCategoryAPI();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    )
  }

  public onEditCategory(category: Category): void {

    this.categoryService.editCategory(category).subscribe(
      (response: Category) => {
        this.getCategoryAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onDeleteCategory(id: number): void {
    this.categoryService.deleteCategory(id).subscribe(
      () => {
        this.getCategoryAPI();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onOpenModal(cat: Category, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button')

    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-bs-toggle', 'modal');

    if (mode === 'add') {
      button.setAttribute('data-bs-target', '#addCategoryModal');
    }

    if (mode === 'edit') {
      this.editCategory = cat;
      button.setAttribute('data-bs-target', '#editCategoryModal');
    }

    if (mode === 'delete') {
      this.deleteCategory = cat;
      button.setAttribute('data-bs-target', '#deleteCategoryModal');
    }

    if(mode == 'delete-selected'){
      button.setAttribute('data-bs-target', '#deleteSelectedCategoryModal');
    }

    container!.appendChild(button);
    button.click();
  }

  public nextPage() {
    console.log()
    if (this.current_page < this.totalPages()) {
      this.current_page++;
      this.getCategoryAPI();
    }
  }

  public prevPage() {
    if (this.current_page > 1) {
      this.current_page--;
      this.getCategoryAPI();
    }
  }

  public totalPages() {
    return Math.ceil(this.category.length / this.records_per_page);
  }

  public isSelected(obj: any, isChecked: boolean) {
    if (isChecked) {
      this.tempSelectedId.push(obj.id);
    } else {
      let filtered = [];
      for (let index = 0; index < this.tempSelectedId.length; index++) {
        if (this.tempSelectedId[index] !== obj.id) {
          filtered.push(this.tempSelectedId[index]);
        }
      }
      this.tempSelectedId = filtered;
    }
    console.log(this.tempSelectedId)
  }

  public isSelectedAll(){
    if(this.selectedAll) {
      this.selectedAll = false;
      this.tempSelectedId = []
    } else {
      this.selectedAll = true;
      for (let index = (this.current_page-1) * this.records_per_page; index < this.records_per_page * this.records_per_page; index++) {
        if(index < this.tempCategory.legth) {
          this.isSelected(this.tempCategory[index], true)
        }
      }
    }
console.log(this.tempSelectedId)
  }

  public onMultipleSelected() {
    if (this.multipleSelectIsEnabled) {
      this.multipleSelectIsEnabled = false;
      this.tempSelectedId = [];
    } else {
      this.multipleSelectIsEnabled = true;
    }
    console.log('multipleSelected ' + this.multipleSelectIsEnabled)
    console.log('selected ' + this.tempSelectedId)
    
  }

  public deleteSelected() {
    for (let i = 0; i < this.tempSelectedId.length; i++) {
      this.onDeleteCategory(this.tempSelectedId[i]);
    }
  }

  public select_records_per_page(value: number) {
    this.records_per_page = value;
    this.getCategoryAPI()
    console.log(this.records_per_page)
  }

  public setSortedBy(value: string) {
    this.sortedBy = value;
    this.sortByCategoryAPI(this.sortedBy, this.mode);
  }

  public setMode(value: string) {
    this.mode = value;
    this.sortByCategoryAPI(this.sortedBy, this.mode);
  }

}
