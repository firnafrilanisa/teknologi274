import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../config/baseurl';
import { Variant } from '../models/variant';

@Injectable({
  providedIn: 'root'
})
export class VariantService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public getVariant(current_page: any, records_per_page: any, sortBy: string, mode: string): Observable<Variant[]> {
    return this.http.get<Variant[]>(
      `${Config.url}api/variant?current_page=${current_page}&records_per_page=${records_per_page}&sortBy=${sortBy}&mode=${mode}`,
      this.httpOptions
    )
  }

  public searchVariant(search : any): Observable<Variant[]> {
    return this.http.get<Variant[]>(
      `${Config.url}api/variant/${search}`,
      this.httpOptions
    )
  }

  public getVariantFromCategory(category_id : any): Observable<Variant[]> {
    return this.http.get<Variant[]>(
      Config.url + 'api/variantFromCategory/' + category_id,
      this.httpOptions
    )
  }

  public addVariant(variant : Variant): Observable<Variant> {
    return this.http.post<Variant>(
      Config.url + 'api/addvariant',
      variant, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public editVariant(variant : Variant): Observable<Variant> {
    return this.http.put<Variant>(
      Config.url + 'api/updatevariant/' + variant.id,
      variant, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

  public deleteVariant(id : number): Observable<void> {
    return this.http.put<void>(
      Config.url + 'api/deletevariant/' + id, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

}
