import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../config/baseurl';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
  };

  public getProduct(): Observable<Product[]>{
    return this.http.get<Product[]>(
      `${Config.url}api/product`,
      this.httpOptions
    )
  }

  public searchProduct(search : any): Observable<Product[]>{
    return this.http.get<Product[]>(
      `${Config.url}api/product/${search}`,
      this.httpOptions
    )
  }

  public getProductById(id : number): Observable<Product>{
    return this.http.get<Product>(
      Config.url + 'api/productby/' + id,
      this.httpOptions,
    )
  }

  public addProduct(product : Product): Observable<Product> {
    return this.http.post<Product>(
      Config.url + 'api/addproduct',
      product, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public editProduct(product : Product): Observable<Product> {
    return this.http.put<Product>(
      Config.url + 'api/updateproduct/' + product.id,
      product, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

  public deleteProduct(id : number): Observable<void> {
    return this.http.put<void>(
      Config.url + 'api/deleteproduct/' + id, {
        ...this.httpOptions,
        responseType: 'text' as 'json'
      }
    )
  }

}
