import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoryComponent } from './pages/category/category.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { CategoryService } from './services/category.service';
import { FormsModule } from '@angular/forms';
import { VariantComponent } from './pages/variant/variant.component';
import { VariantService } from './services/variant.service';
import { ProductComponent } from './pages/product/product.component';
import { OrderComponent } from './pages/order/order.component';
import { ProductService } from './services/product.service';
import { OrderService } from './services/order.service';

@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    NavbarComponent,
    HomeComponent,
    VariantComponent,
    ProductComponent,
    OrderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    CategoryService,
    VariantService, 
    ProductService, 
    OrderService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
