const { request, response } = require("express");

module.exports = exports = (app, pool) => {

    app.get('/api/variant', (request, response) => {
        let qPage = request.query.current_page
        let qPer_page = request.query.records_per_page

        let records_per_page = parseInt(qPer_page)
        let current_page = parseInt(qPage)
        let sortBy = request.query.sortBy
        let mode = request.query.mode

        records_per_page = qPer_page === undefined ? 5 : qPer_page
        current_page = qPage === undefined ? 0 : (current_page - 1) * records_per_page

        let pages = qPage === undefined ? 1 : qPage

        const query = `SELECT v.id, v.variant_code, v.variant_name, c.id AS category_id, 
        c.category_name, v.is_active FROM variant v JOIN category c 
        ON v.category_id = c.id WHERE v.is_active = true ORDER BY ${sortBy} ${mode}
        LIMIT ${records_per_page} OFFSET ${current_page}`

        const query2 = `SELECT v.id, v.variant_code, v.variant_name, c.id AS category_id, 
        c.category_name, v.is_active FROM variant v JOIN category c 
        ON v.category_id = c.id WHERE v.is_active = true ORDER BY variant_code`

        pool.query(query2, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                let totalItems = result.rowCount;
                pool.query(query, (error2, result2) => {
                    if (error2) {
                        response.send(400, {
                            success: false,
                            result: error2,
                        });
                    } else {
                        response.send(200, {
                            current_page: parseInt(pages),
                            records_per_page: parseInt(records_per_page),
                            total: totalItems,
                            result: result2.rows,
                            success: true,
                        })
                    }
                })
            }
        })
    });

    app.get('/api/variant/:search', (request, response) => {
        const search = request.params.search;
        const query = `SELECT *
        FROM variant v
        JOIN category c
        ON c.id = v.category_id
        WHERE LOWER(v.variant_name) LIKE LOWER('%${search}%') AND v.is_active = true
        ORDER BY v.variant_code`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    })

    app.get('/api/variantFromCategory/:category_id', (request, response) => {
        const category_id = request.params.category_id;
        const query = `SELECT v.id as variant_id, v.variant_code, v.variant_name, v.category_id, c.category_name FROM variant v JOIN category c ON v.category_id = c.id WHERE v.is_active = true AND v.category_id = ${category_id} ORDER BY v.variant_code`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    })

    app.get('/api/variant/:id', (request, response) => {
        const { id } = request.params;
        const query = `SELECT * FROM variant WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0]
                });
            }
        });
    });

    app.post('/api/addvariant', (request, response) => {
        const { variant_code, variant_name, category_id } = request.body;
        const query = `INSERT INTO public.variant(variant_code, variant_name, created_by, created_date, is_active, category_id)
        VALUES ('${variant_code}', '${variant_name}', 'user1', 'now()', true, '${category_id}')`;

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Variant ${variant_name} saved`
                });
            }
        });
    });

    app.put('/api/updatevariant/:id', (request, response) => {
        const { variant_code, variant_name, category_id } = request.body;
        const id = request.params.id;

        const query = `UPDATE variant SET variant_code = '${variant_code}', variant_name = '${variant_name}', category_id = '${category_id}', updated_by = 'user1', updated_date = 'now()' WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Variant ${variant_code} updated`
                });
            }
        });
    });

    app.put('/api/deletevariant/:id', (request, response) => {
        const { is_active } = request.body;
        const id = request.params.id;

        const query = `UPDATE variant SET is_active = 'false', updated_by = 'user1', updated_date = 'now()' WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Variant ${id} deleted`
                });
            }
        });
    });

};