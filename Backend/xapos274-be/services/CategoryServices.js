const { request, response } = require("express");

module.exports = exports = (app, pool) => {//app menyambungkan ke server

    app.get('/api/category', (request, response) => {//pool nyambungin ke database
        const query = `SELECT * FROM category WHERE is_active = true ORDER BY category_code ASC`;

        pool.query(query, (error, result) => {
            // console.log(result)
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/category/:sortedBy/:mode', (request, response) => {//pool nyambungin ke database
        const sortedBy = request.params.sortedBy;
        const mode = request.params.mode;
        const query = `SELECT * FROM category WHERE is_active = true ORDER BY ${sortedBy} ${mode}`;

        pool.query(query, (error, result) => {
            // console.log(result)
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/category/:search', (request, response) => {
        const search = request.params.search;
        const query = `SELECT * FROM category
        WHERE LOWER(category_name) LIKE LOWER('%${search}%') AND is_active = true ORDER BY category_code ASC`

        pool.query(query, (error, result) => {
            if(error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    })

    app.get('/api/category/:id', (request, response) => {//: menandakan suatu parameter = params
        //ada 2 cara memanggil id can query

        // const ids = request.params.id;
        // const query = 'SELECT * FROM category WHERE id = '+ids;

        const {id} = request.params;     
        const query = `SELECT * FROM category WHERE id = ${id}`

        // console.log(request)
 
        pool.query(query, (error, result) => {
            // console.log(error)
            if(error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0] //agar hasil tidak berupa array
                })
            }
        })

    })

    app.post('/api/addcategory', (request, response) => {
        const {category_code, category_name} = request.body;

        const query = `INSERT INTO public.category(
            category_code, category_name, create_by, create_date, is_active)
            VALUES ('${category_code}', '${category_name}', 'user1', 'now()', true)`;

            pool.query(query, (error, result) => {
                if(error) {
                    response.send(400, {
                        success: false,
                        result: error,
                    });
                } else {
                    response.send(200, {
                        success: true,
                        result: `Data Category ${category_name} saved`
                    });
                }
            });
    });

    app.put('/api/updatecategory/:id', (request, response) => {
        const {category_code, category_name} = request.body;
        const id = request.params.id;
        
        const query = `UPDATE category SET category_code = '${category_code}', category_name = '${category_name}',
        modify_date = 'now()', modify_by = 'user1' WHERE id = ${id}`
        
        pool.query(query, (error, result) => {
            if(error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Category ${id} updated`
                });
            }
        });
    });

    app.put('/api/deletecategory/:id', (request, response) => {
        const {is_active} = request.body;
        const id = request.params.id;
        
        const query = `UPDATE category SET is_active = 'false', modify_date = 'now()', modify_by = 'user1' WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if(error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Category ${id} deleted`
                });
            }
        });
    });


};

