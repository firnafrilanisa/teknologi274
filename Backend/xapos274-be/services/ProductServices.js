const { request, response } = require("express");

module.exports = exports = (app, pool) => {
    app.get('/api/product', (request, response) => {
        const query = 'SELECT p.id, p.product_code, p.product_name, p.product_price, p.product_stock, p.variant_id, v.variant_name, c.id AS category_id, c.category_name FROM product p JOIN variant v ON p.variant_id = v.id JOIN category c ON c.id = v.category_id WHERE p.is_active = true ORDER BY p.product_code';

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/product/:search', (request, response) => {
        const search = request.params.search;
        const query = `SELECT p.id, p.product_code, p.product_name, p.product_price, 
        p.product_stock, p.variant_id, v.variant_name, c.id AS category_id, c.category_name 
        FROM product p 
        JOIN variant v 
        ON p.variant_id = v.id 
        JOIN category c 
        ON c.id = v.category_id 
        WHERE LOWER(p.product_name) LIKE LOWER('%${search}%') AND p.is_active = true 
        ORDER BY p.product_code`
        
        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows,
                });
            }
        });
    });

    app.get('/api/productby/:id', (request, response) => {
        const id = request.params.id
        const query = `SELECT * FROM product WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                })
            } else {
                response.send(200, {
                    success: true,
                    result: result.rows[0],
                });
            }
        });
    });

    app.post('/api/addproduct', (request, response) => {
        const { product_code, product_name, product_price, product_stock, variant_id} = request.body;
        const query = `INSERT INTO public.product(product_code, product_name, product_price, product_stock, variant_id, created_by, created_date, is_active)
        VALUES ('${product_code}', '${product_name}', '${product_price}', '${product_stock}', '${variant_id}','user1', 'now()', true)`;

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error,
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Product ${product_name} saved`
                });
            }
        });
    });

    app.put('/api/updateproduct/:id', (request, response) => {
        const {product_code, product_name, product_price, product_stock, variant_id} = request.body;
        const id = request.params.id;

        const query = `UPDATE product SET product_code = '${product_code}', product_name = '${product_name}', product_price = '${product_price}', product_stock = '${product_stock}', variant_id = '${variant_id}', updated_by = 'user1', updated_date = 'now()' WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Product ${product_code} updated`
                });
            }
        });
    });

    app.put('/api/deleteproduct/:id', (request, response) => {
        const { is_active } = request.body;
        const id = request.params.id;

        const query = `UPDATE product SET is_active = 'false', updated_by = 'user1', updated_date = 'now()' WHERE id = ${id}`

        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                });
            } else {
                response.send(200, {
                    success: true,
                    result: `Data Product ${id} deleted`
                });
            }
        });
    });
}