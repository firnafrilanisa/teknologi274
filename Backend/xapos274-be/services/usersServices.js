const { request, response } = require("express")

//CRUD Create Read Update Delete 
module.exports = exports = (app, pool) => {
    app.get('/api/users', (request, response) => {
        pool.query('SELECT * FROM users ORDER BY id', (error, result) => { //ngambil
            if (error) {
                throw error
            } else {
                response.status(200).json(result.rows)
            }
        })
    })

    app.post('/api/addusers', (request, response) => { //ngirim
        const { name, email } = request.body//memakai `` untuk menambahkan data karena perlu concat
        const query = `INSERT INTO public.users(name, email)
                    VALUES ('${name}', '${email}')`
        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: `Data ${name} saved`
                })
            }
        })
    })


    app.put('/api/update/:id', (request, response) => {//memperbaharui
        const { name, email } = request.body
        const id = parseInt(request.params.id)
        const query = `UPDATE users SET name = '${name}',email = '${email}' WHERE id = '${id}'`
        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: `Data ${name} saved`
                })
            }
        })
    })

    app.delete('/api/delete/:id', (request, response) => {//menghapus
        const { name, email } = request.body
        const id = parseInt(request.params.id)
        const query = `DELETE FROM users WHERE id = '${id}'`
        pool.query(query, (error, result) => {
            if (error) {
                response.send(400, {
                    success: false,
                    result: error
                })
            } else {
                response.send(200, {
                    success: true,
                    result: `Data ${name} saved`
                })
            }
        })
    })

}
